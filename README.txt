Some quick information about this project

To get the thing running:
- `pip install -r requirements.txt`
- Create the config file (copy config.py.example to config.py and add the missing information)
- `python run_server.py`

Was built with/for python2, but should (in theory) work with python3 too. (TODO: Port to Python3)
- Uses Flask for the main thing
- Uses SQLAlchemy to communicate with a mysql database
- Uses Jinja2 for the templating
- Uses Flask-Login for a secure logging in system (But still, see the # TODO comments in ficsrecs/views.py)
- Uses Flask-JSGlue to support Flask's url_for() function in JavaScript
- Uses WTForms to provide forms

run_server.py - starts a server running the application, used to prepare for production
frontend/frontend.html - copy paste it in a tumblr page to see the system running
ficrecs/__init__.py - this is the actual system; imports everything, starts a flask app and configures routing
ficrecs/config.py - the config for the application, loaded by __init__.py
ficrecs/exceptions.py - the custom exceptions, mostly error pages for the flask app
ficrecs/forms.py - define all the forms used; is a bit hacky now and then, FormInsecure is basically a Form without CSRF protection, so useful for custom madness, such as combined form elements: Select on Fandom, which lists all the ships in a different Select
ficrecs/models.py - the database models; every table is a subclass of db.Model, where the db.Table thingies are a bit special: they define many-to-many relationships, and their are an aweful lot of those in this project
ficrecs/views.py - this is where everything happens really, every page is defined here. Every class whose name starts with Editor is part of the backend, every class starting with API provides the API. This file is huge, with over 1100 lines of code. Please ignore APIWorksSortByWordCount, APIWorksByShip, APIWorksByShipSortByWordCount and APIWorksByFandomSortByWordCount, as those have yet to be updated to the new system of putting stickied works to the top. The current code gives a missing parameter type of syntax error when accessing the pages.

That's about it, I think. Have fun :)