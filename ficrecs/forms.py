# encoding=utf-8
from __future__ import absolute_import

from wtforms.form import Form as FormInsecure
from flask_wtf import Form
from wtforms import TextAreaField, BooleanField, StringField, PasswordField, SelectField, FormField, FieldList
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea

from ficrecs import db
from ficrecs.models import Fandom, Character, Relationship, Author


# Helpers
BS3_ATTRIBUTES = ['help_text']


class BS3Helper(object):
    def __init__(self, help_text=None):
        self.help_text = help_text


# Fields
class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()

    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(CKTextAreaField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3StringField(StringField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3StringField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3SelectField(SelectField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3SelectField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3SelectFieldCharacterByFandom(BS3SelectField):
    def __init__(self, **kwargs):
        super(BS3SelectFieldCharacterByFandom, self).__init__(**kwargs)

    def pre_validate(self, form):
        if hasattr(form, 'fandom'):
            fandom_id = form.fandom.data
            characters = Character.query.filter(Character.fandom_id == fandom_id).all()
            self.choices = [(c.id, c.name) for c in characters]
            super(BS3SelectFieldCharacterByFandom, self).pre_validate(form)


class BS3SelectFieldShipByFandom(BS3SelectField):
    def __init__(self, **kwargs):
        super(BS3SelectFieldShipByFandom, self).__init__(**kwargs)

    def pre_validate(self, form):
        if hasattr(form, 'fandom'):
            fandom_id = form.fandom.data
            ships = db.session.query(Relationship).join(Relationship.fandoms).filter(Fandom.id == fandom_id).all()
            self.choices = [(s.id, s.name) for s in ships]
            super(BS3SelectFieldShipByFandom, self).pre_validate(form)


class BS3SelectFieldAuthorByFandom(BS3SelectField):
    def __init__(self, **kwargs):
        super(BS3SelectFieldAuthorByFandom, self).__init__(**kwargs)

    def pre_validate(self, form):
        if hasattr(form, 'fandom'):
            fandom_id = form.fandom.data
            authors = db.session.query(Author).join(Author.fandoms).filter(Fandom.id == fandom_id).all()
            self.choices = [(a.id, a.name) for a in authors]
            super(BS3SelectFieldAuthorByFandom, self).pre_validate(form)


class BS3PasswordField(PasswordField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3PasswordField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3BooleanField(BooleanField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3BooleanField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


# Forms
class EditorFandomHelperForm(FormInsecure):
    fandom = BS3SelectField(label='Fandom: ', coerce=int, validators=[DataRequired(message='This is a required field')])

    def __init__(self, *args, **kwargs):
        # NOTE TO SELF: When copying this in the future, make sure to add the choices on view level too
        fandoms = Fandom.query.order_by('name').all()
        self.fandom.choices = [(f.id, f.name) for f in fandoms]

        super(EditorFandomHelperForm, self).__init__(*args, **kwargs)


class EditorCharacterSelectHelperForm(FormInsecure):
    fandom = BS3SelectField(label='Fandom: ', coerce=int)
    character = BS3SelectFieldCharacterByFandom(label='Character: ', coerce=int)

    def __init__(self, *args, **kwargs):
        fandoms = Fandom.query.order_by('name').all()
        self.fandom.choices = [(f.id, f.name) for f in fandoms]

        characters = Character.query.filter(Character.fandom_id == fandoms[0].id).all()
        self.character.choices = [(c.id, c.name) for c in characters]

        super(EditorCharacterSelectHelperForm, self).__init__(*args, **kwargs)


class EditorShipsSelectHelperForm(FormInsecure):
    fandom = BS3SelectField(label='Fandom: ', coerce=int)
    ship = BS3SelectFieldShipByFandom(label='Ship: ', coerce=int)

    def __init__(self, *args, **kwargs):
        fandoms = Fandom.query.order_by('name').all()
        self.fandom.choices = [(f.id, f.name) for f in fandoms]

        ships = db.session.query(Relationship).join(Relationship.fandoms).filter(Fandom.id == fandoms[0].id).all()
        self.ship.choices = [(s.id, s.name) for s in ships]

        super(EditorShipsSelectHelperForm, self).__init__(*args, **kwargs)


class EditorAuthorSelectHelperForm(FormInsecure):
    fandom = BS3SelectField(label='Fandom: ', coerce=int)
    author = BS3SelectFieldAuthorByFandom(label='Author: ', coerce=int)

    def __init__(self, *args, **kwargs):
        fandoms = Fandom.query.order_by('name').all()
        self.fandom.choices = [(f.id, f.name) for f in fandoms]

        authors = db.session.query(Author).join(Author.fandoms).filter(Fandom.id == fandoms[0].id).all()
        self.author.choices = [(a.id, a.name) for a in authors]

        super(EditorAuthorSelectHelperForm, self).__init__(*args, **kwargs)


class EditorLoginForm(Form):
    username = BS3StringField(label='Username: ', validators=[DataRequired(message='This is a required field')])
    password = BS3PasswordField(label='Password: ', validators=[DataRequired(message='This is a required field')])
    remember = BS3BooleanField(label='Remember me?')


class EditorFandomForm(Form):
    name = BS3StringField(label='Name: ', validators=[DataRequired(message='This is a required field')])
    comments = CKTextAreaField(label='Comments: ')


class EditorCharacterForm(Form):
    fandom = BS3SelectField(label='Fandom: ', coerce=int, validators=[DataRequired(message='This is a required field')])
    character_name = BS3StringField(label='Character name: ', validators=[
        DataRequired(message='This is a required field')
    ])


class EditorAuthorForm(Form):
    fandoms = FieldList(FormField(EditorFandomHelperForm), 'Fandoms: ', validators=[
        DataRequired(message='This is a required field')
    ], min_entries=1)
    author_name = BS3StringField(label='Author name: ', validators=[
        DataRequired(message='This is a required field')
    ])
    author_url = BS3StringField(label='Author URL: ')
    comments = CKTextAreaField(label='Comments: ')

# class EditorRelationshipForm(Form):
#     ship_name = BS3StringField(label='Ship name: ')
#     comments = CKTextAreaField(label='Comments: ')
#
#     characters = FieldList(FormField(EditorCharacterSelectHelperForm), 'Characters: ', validators=[
#         DataRequired(message='This is a required field')
#     ], min_entries=1)


class EditorRelationshipForm(Form):
    ship_name = BS3StringField(label='Ship name: ')
    comments = CKTextAreaField(label='Comments: ')

    characters = FieldList(FormField(EditorCharacterSelectHelperForm), 'Characters: ', validators=[
        DataRequired(message='This is a required field')
    ], min_entries=2)


class EditorWorkForm(Form):
    title = BS3StringField(label='Title: ', validators=[DataRequired(message='This is a required field')])
    url = BS3StringField(label='URL: ', validators=[DataRequired(message='This is a required field')])
    description = CKTextAreaField(label='Description: ')
    rating = BS3SelectField(
        label='Rating: ',
        choices=[('UR', 'Unrated'),
                 ('G', 'Gen'),
                 ('PG', 'PG'),
                 ('PG-13', 'PG-13'),
                 ('15', '15'),
                 ('R', 'R'),
                 ('NC-17', 'NC-17')
                 ],
        validators=[DataRequired(message='This is a required field')]
    )
    spoilers = CKTextAreaField(label='Spoilers: ')
    complete = BS3BooleanField(label='Complete? ')
    sticky = BS3BooleanField(label='Sticky? ')

    tags = BS3StringField(label='Tags: ')

    word_count = BS3StringField(label='Word count: ')
    status = BS3StringField(label='Work status: ')
    setting = BS3StringField(label='Setting: ')
