# encoding=utf-8
from __future__ import absolute_import
import json

from markupsafe import Markup
from itsdangerous import Serializer

from flask import render_template, redirect, request, url_for, flash, make_response
from flask.views import View, MethodView

from flask_login import login_user, logout_user, login_required

from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql.expression import false, func

from ficrecs import db

# from ficrecs.database import db_session
from ficrecs.exceptions import MalformedRequestException
from ficrecs.forms import (EditorLoginForm, EditorFandomForm, EditorCharacterForm, EditorFandomHelperForm,
                           EditorCharacterSelectHelperForm, EditorRelationshipForm, EditorAuthorForm,
                           EditorShipsSelectHelperForm, EditorAuthorSelectHelperForm, EditorWorkForm)
from ficrecs.models import Fandom, Character, Relationship, Work, Tag, User, Author, fandoms_works


class Index(View):
    endpoint = 'index'

    def dispatch_request(self):
        return self.endpoint


# Editor ===============================================================================================================
class EditorIndex(View):
    endpoint = 'editor_index'
    decorators = [login_required]

    def dispatch_request(self):
        return _editor_render_template('index.html')


class EditorLogin(View):
    endpoint = 'editor_login'

    def dispatch_request(self):
        form = EditorLoginForm()
        if form.validate_on_submit():
            user = User.query.filter(User.email == form.username.data).first()
            if user is None:
                # User is not known, flash message and return
                flash(u'No user found with this email address.', 'error')
                return render_template('login.html', form=form)
            if user.validate_password(form.password.data):
                # Valid login
                login_user(user, form.remember.data)

                next_ = request.args.get('next')
                # TODO: validate `next_`
                return redirect(next_ or url_for('editor_index'))
            else:
                flash(u'This combination of email address and password is not known', 'error')
                return render_template('login.html', form=form)
        return render_template('login.html', form=form)


class EditorLogout(View):
    endpoint = 'editor_logout'
    decorators = [login_required]

    def dispatch_request(self):
        logout_user()
        return redirect(url_for(EditorLogin.endpoint))


class EditorFandom(MethodView):
    endpoint = 'editor_fandom'
    decorators = [login_required]

    def get(self):
        fandoms = Fandom.query.order_by('name').all()
        return _editor_render_template('fandoms.html', fandoms=fandoms)


class EditorFandomEdit(MethodView):
    endpoint = 'editor_fandom_edit'
    decorators = [login_required]

    def get(self, fandom_id):
        form = EditorFandomForm()

        if fandom_id == -1:
            # New Fandom
            pass
        else:
            # Pull existing info from db
            res = Fandom.query.filter(Fandom.id == fandom_id).first()
            if not res:
                # id does not exist; choice: new fandom, redirect; for now: redirect
                return redirect(url_for(EditorFandom.endpoint))

            form.name.data = res.name
            form.comments.data = res.comments

        return _editor_render_template('fandoms_edit.html', form=form, id=fandom_id)

    def post(self, fandom_id):
        form = EditorFandomForm()

        if fandom_id == -1:
            # New fandom
            pass
        else:
            # Pull existing info from db
            res = Fandom.query.filter(Fandom.id == fandom_id).first()
            if not res:
                # id does not exist; choice: new fandom, redirect; for now: redirect
                return redirect(url_for('editor_fandom'))

        if form.validate_on_submit():
            if fandom_id == -1:
                fandom = Fandom(name=form.name.data, comments=form.comments.data)
                db.session.add(fandom)
            else:
                fandom = Fandom.query.filter(Fandom.id == fandom_id).first()
                fandom.name = form.name.data
                fandom.comments = form.comments.data

            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
                flash(u'A fandom with this name is already known.', 'error')
                return redirect(url_for('editor_fandom'))

            flash('Fandom created', category='info')
            return redirect(url_for('editor_fandom'))

        return _editor_render_template('fandoms_edit.html', form=form, id=fandom_id)


class EditorCharacter(MethodView):
    endpoint = 'editor_character'
    decorators = [login_required]

    @staticmethod
    def _prepare_form():
        form = EditorFandomHelperForm()
        fandoms = Fandom.query.order_by('name').all()
        form.fandom.choices = [(fandom.id, fandom.name) for fandom in fandoms]
        return form

    def get(self):
        form = self._prepare_form()
        return _editor_render_template('characters.html', form=form)


class EditorCharacterEdit(MethodView):
    endpoint = 'editor_character_edit'
    decorators = [login_required]

    @staticmethod
    def _prepare_form():
        form = EditorCharacterForm()
        fandoms = Fandom.query.order_by('name').all()
        form.fandom.choices = [(fandom.id, fandom.name) for fandom in fandoms]
        return form

    def get(self, character_id):
        form = self._prepare_form()

        if character_id == -1:
            # New character
            pass
        else:
            # Get existing character's info from db
            res = Character.query.filter(Character.id == character_id).first()
            if not res:
                # Character does not exist, choice between new character or redirect; for now choose for redirect
                return redirect(url_for('editor_character'))

            form.character_name.data = res.name
            form.fandom.data = res.fandom_id

        return _editor_render_template('characters_edit.html', form=form, id=character_id)

    def post(self, character_id):
        form = self._prepare_form()
        if character_id == -1:
            # New character
            pass
        else:
            # Get existing character's info from db
            res = Character.query.filter(Character.id == character_id).first()
            if not res:
                # Character does not exist, choice between new character or redirect; for now choose for redirect
                return redirect(url_for('editor_character'))

        if form.validate_on_submit():
            if character_id == -1:
                character = Character(name=form.character_name.data, fandom_id=form.fandom.data)
                db.session.add(character)
            else:
                character = Character.query.filter(Character.id == character_id).first()
                if not character:
                    # malformed request; character does not yet exist
                    raise MalformedRequestException()
                else:
                    character.name = form.character_name.data
                    character.fandom_id = form.fandom.data

            db.session.commit()

            flash('Character created', category='info')
            return redirect(url_for('editor_character'))

        return _editor_render_template('characters_edit.html', form=form, id=character_id)


class EditorShips(MethodView):
    endpoint = 'editor_ship'
    decorators = [login_required]

    @staticmethod
    def _prepare_form():
        form = EditorFandomHelperForm()
        fandoms = Fandom.query.order_by('name').all()
        form.fandom.choices = [(fandom.id, fandom.name) for fandom in fandoms]
        return form

    def get(self):
        form = self._prepare_form()
        return _editor_render_template('ships.html', form=form)


class EditorShipsEdit(MethodView):
    endpoint = 'editor_ship_edit'
    decorators = [login_required]
    min_characters = 2

    def get(self, ship_id):
        form = EditorRelationshipForm()

        fandoms = Fandom.query.order_by('name').all()
        fandom_choices = [(fandom.id, fandom.name) for fandom in fandoms]

        if ship_id == -1:
            # New ship; prepare form with default entries

            # Get characters for first listed fandom
            characters = Character.query.filter(Character.fandom_id == fandoms[0].id).all()

            for entry in form.characters.entries:
                entry.fandom.choices = fandom_choices
                entry.character.choices = [(c.id, c.name) for c in characters]
        else:
            # Get info from DB
            res = Relationship.query.filter(Relationship.id == ship_id).first()
            if not res:
                # Ship does not exists, 2 choices: either redirect to index or give empty form; for now redirect
                return redirect(url_for('editor_ship'))

            # Fill form based on DB output
            form.ship_name.data = res.name
            form.comments.data = res.comments

            # Remove empty entries of 'characters'
            for _ in range(0, form.characters.min_entries):
                form.characters.pop_entry()

            for character in res.characters:
                helper_form = EditorCharacterSelectHelperForm()

                helper_form.fandom = character.fandom_id
                helper_form.character = character.id

                form.characters.append_entry(helper_form)

            for entry in form.characters.entries:
                entry.fandom.choices = fandom_choices
                entry.fandom.default = entry.fandom.data

                characters = Character.query.filter(Character.fandom_id == entry.fandom.data).all()
                entry.character.choices = [(c.id, c.name) for c in characters]
                entry.character.default = entry.character.data

        return _editor_render_template('ships_edit2.html', form=form, id=ship_id, min_characters=self.min_characters)

    def post(self, ship_id):
        form = EditorRelationshipForm()

        fandoms = Fandom.query.order_by('name').all()
        fandom_choices = [(fandom.id, fandom.name) for fandom in fandoms]

        for entry in form.characters.entries:
            entry.fandom.choices = fandom_choices

            # characters = Character.query.filter(Character.fandom_id == entry.fandom.data).all()
            # entry.character.choices = [(c.id, c.name) for c in characters]

        if ship_id == -1:
            # New ship
            pass
        else:
            # Get info from DB
            res = Relationship.query.filter(Relationship.id == ship_id)
            if not res:
                # Ship does not exists, 2 choices: either redirect to index or give empty form; for now redirect
                return redirect(url_for('editor_ship'))

        if form.validate_on_submit():
            if ship_id == -1:
                ship = Relationship(form.ship_name.data, form.comments.data)
                for entry in form.characters.entries:
                    character = Character.query.filter(Character.id == entry.character.data).first()
                    fandom = Fandom.query.filter(Fandom.id == character.fandom_id).first()
                    ship.characters.append(character)
                    if fandom not in ship.fandoms:
                        ship.fandoms.append(fandom)

                db.session.add(ship)
                flash('Ship created', category='info')
            else:
                ship = Relationship.query.filter(Relationship.id == ship_id).first()
                ship.name = form.ship_name.data
                ship.comments = form.comments.data

                form_characters = []
                form_fandoms = set()

                for entry in form.characters.entries:
                    character = Character.query.filter(Character.id == entry.character.data).first()
                    form_characters.append(character)

                    fandom = Fandom.query.filter(Fandom.id == character.fandom_id).first()
                    form_fandoms.add(fandom)

                for character in ship.characters:
                    if character not in form_characters:
                        # Existed in ship before, not any longer
                        ship.characters.remove(character)
                        print(u'Removed from ship: {0}'.format(character.name))
                    else:
                        # Already exists in ship
                        form_characters.remove(character)
                        print(u'Exists already in ship: {0}'.format(character.name))

                for character in form_characters:
                    # Only characters who aren't already in ship will be listed here
                    ship.characters.append(character)
                    print(u'New in ship: {0}'.format(character.name))

                for fandom in ship.fandoms:
                    if fandom not in form_fandoms:
                        # Existed in ship before, not any longer
                        ship.fandoms.remove(fandom)
                    else:
                        # Already exists in ship
                        form_fandoms.remove(fandom)

                for fandom in form_fandoms:
                    # Only fandoms that aren't already in ship will be listed here, but might be doubled
                    ship.fandoms.append(fandom)

                flash('Ship edited', category='info')

            db.session.commit()

            return redirect(url_for('editor_ship'))
        else:
            return _editor_render_template('ships_edit2.html', form=form, id=ship_id,
                                           min_characters=self.min_characters)


class EditorAuthor(MethodView):
    endpoint = 'editor_author'
    decorators = [login_required]

    @staticmethod
    def _prepare_form():
        form = EditorFandomHelperForm()
        fandoms = Fandom.query.order_by('name').all()
        form.fandom.choices = [(fandom.id, fandom.name) for fandom in fandoms]
        return form

    def get(self):
        form = self._prepare_form()
        return _editor_render_template('authors.html', form=form)


class EditorAuthorEdit(MethodView):
    endpoint = 'editor_author_edit'
    decorators = [login_required]
    min_fandoms = 1

    def get(self, author_id):
        form = EditorAuthorForm()

        fandoms = Fandom.query.order_by('name').all()
        fandom_choices = [(fandom.id, fandom.name) for fandom in fandoms]

        if author_id == -1:
            # New author; prepare form with default entries
            for entry in form.fandoms.entries:
                entry.fandom.choices = fandom_choices
        else:
            # Get existing author's info from db
            res = Author.query.filter(Author.id == author_id).first()
            if not res:
                # Author does not exist, choice between new author or redirect; for now choose for redirect
                return redirect(url_for('editor_author'))

            form.author_name.data = res.name
            form.author_url.data = res.url
            form.comments.data = res.comments

            # Remove empty entries of 'fandoms'
            for _ in range(0, form.fandoms.min_entries):
                form.fandoms.pop_entry()

            for fandom in res.fandoms:
                helper_form = EditorFandomHelperForm()

                helper_form.fandom = fandom.id

                form.fandoms.append_entry(helper_form)

            for entry in form.fandoms.entries:
                entry.fandom.choices = fandom_choices
                entry.fandom.default = entry.fandom.data

        return _editor_render_template('authors_edit.html', form=form, id=author_id, min_fandoms=self.min_fandoms)

    def post(self, author_id):
        form = EditorAuthorForm()

        fandoms = Fandom.query.order_by('name').all()
        fandom_choices = [(fandom.id, fandom.name) for fandom in fandoms]

        for entry in form.fandoms.entries:
            entry.fandom.choices = fandom_choices

        if author_id == -1:
            # New author
            pass
        else:
            # Get existing author's info from db
            res = Author.query.filter(Author.id == author_id).first()
            if not res:
                # Author does not exist, choice between new author or redirect; for now choose for redirect
                return redirect(url_for('editor_author'))

        if form.validate_on_submit():
            if author_id == -1:
                author = Author(name=form.author_name.data, url=form.author_url.data, comments=form.comments.data)

                for entry in form.fandoms.entries:
                    fandom = Fandom.query.filter(Fandom.id == entry.fandom.data).first()
                    if fandom not in author.fandoms:
                        author.fandoms.append(fandom)

                db.session.add(author)

                flash('Author created', category='info')
            else:
                author = Author.query.filter(Author.id == author_id).first()
                if not author:
                    # malformed request; character does not yet exist
                    raise MalformedRequestException()
                else:
                    author.name = form.author_name.data
                    author.url = form.author_url.data
                    author.comments = form.comments.data

                    form_fandoms = set()

                    for entry in form.fandoms.entries:
                        fandom = Fandom.query.filter(Fandom.id == entry.fandom.data).first()
                        form_fandoms.add(fandom)

                    for fandom in author.fandoms:
                        if fandom not in form_fandoms:
                            # Existed 'in' author before, not any longer
                            author.fandoms.remove(fandom)
                        else:
                            # Already exists 'in' author
                            form_fandoms.remove(fandom)

                    for fandom in form_fandoms:
                        # Only fandoms that aren't already 'in' author will be listed here, but might be doubled
                        author.fandoms.append(fandom)

                    flash('Author edited', category='info')

            db.session.commit()

            return redirect(url_for('editor_author'))

        return _editor_render_template('authors_edit.html', form=form, id=author_id, min_fandoms=self.min_fandoms)


class EditorWork(MethodView):
    endpoint = 'editor_work'
    decorators = [login_required]

    @staticmethod
    def _prepare_form():
        form = EditorFandomHelperForm()
        fandoms = Fandom.query.order_by('name').all()
        form.fandom.choices = [(fandom.id, fandom.name) for fandom in fandoms]
        return form

    def get(self):
        form = self._prepare_form()
        return _editor_render_template('works.html', form=form)


class EditorWorkEdit(MethodView):
    # Welcome to hell. Here, have a cookie
    endpoint = 'editor_work_edit'
    decorators = [login_required]

    min_fandoms = 1
    min_ships = 1
    min_authors = 1

    serializer = Serializer('form_extra_serializer')

    def get(self, work_id):
        form = EditorWorkForm()
        form_extra = {
            'fandoms': [],
            'ships': [],
            'authors': []
        }

        if work_id == -1:
            # New work
            pass
        else:
            # Existing work; pull info from DB
            work = Work.query.filter(Work.id == work_id).first()
            if not work:
                flash('Work not found', category='error')
                return redirect(url_for('editor_work'))

            # Fill form based on DB data
            form.title.data = work.title
            form.url.data = work.url
            form.description.data = work.description
            form.rating.data = work.rating
            form.rating.default = work.rating
            form.spoilers.data = work.spoilers
            form.complete.data = work.complete
            form.sticky.data = work.sticky

            form.tags.data = u', '.join([tag.name for tag in work.tags])

            form.word_count.data = work.word_count
            form.status.data = work.status
            form.setting.data = work.setting

            # Dynamic fields
            # Fandom:
            for fandom in work.fandoms:
                form_extra['fandoms'].append((fandom.id, fandom.name))

            # Ships:
            for ship in work.ships:
                t = u'/'.join(c.name for c in ship.characters)
                if ship.name != u'' and ship.name is not None:
                    t += u' ({0})'.format(ship.name)

                form_extra['ships'].append((ship.id, t))

            # Authors:
            for author in work.authors:
                form_extra['authors'].append((author.id, author.name))

        return _editor_render_template('works_edit.html', form=form, id=work_id, min_fandoms=self.min_fandoms,
                                       min_ships=self.min_ships, min_authors=self.min_authors,
                                       existing='new' if work_id == -1 else 'existing', form_extra=form_extra,
                                       s_form_extra=self.serializer.dumps(form_extra))
        # TODO: check on min_bla in post

    def post(self, work_id):
        form = EditorWorkForm()
        form_extra = self.serializer.loads(request.form.get('s_form_extra'))

        if work_id == -1:
            # New work
            pass
        else:
            # Get info from DB
            res = Work.query.filter(Work.id == work_id).first()
            if not res:
                raise MalformedRequestException()

        if form.validate_on_submit():
            if work_id == -1:
                # New work
                work = Work(
                    form.title.data, form.url.data, form.rating.data, form.description.data, form.spoilers.data,
                    form.complete.data, form.word_count.data, form.status.data, form.setting.data, form.sticky.data
                )

                for fandom in request.form.getlist('fandoms'):
                    fandom = Fandom.query.filter(Fandom.id == int(fandom)).first()
                    if not fandom:
                        raise MalformedRequestException()
                    if fandom not in work.fandoms:
                        work.fandoms.append(fandom)

                for ship in request.form.getlist('ships'):
                    ship = Relationship.query.filter(Relationship.id == int(ship)).first()
                    if not ship:
                        raise MalformedRequestException()
                    if ship not in work.ships:
                        work.ships.append(ship)

                for author in request.form.getlist('authors'):
                    author = Author.query.filter(Author.id == int(author)).first()
                    if not author:
                        raise MalformedRequestException()
                    if author not in work.authors:
                        work.authors.append(author)

                # Tags:
                form_tags = set()
                tags = form.tags.data
                if tags != '' and tags is not None:
                    # Tags entered
                    l_tags = tags.split(',')
                    for tag in l_tags:
                        tag = tag.strip()
                        db_tag = Tag.query.filter(Tag.name == tag).first()
                        if not db_tag:
                            # New tag
                            db_tag = Tag(tag)
                            db.session.add(db_tag)
                        form_tags.add(db_tag)

                for tag in form_tags:
                    # Add the tags to the work
                    work.tags.append(tag)

                db.session.add(work)
                flash('Work created', category='info')
            else:
                # Pull info from DB
                work = Work.query.filter(Work.id == work_id).first()

                # Populate model from form
                work.title = form.title.data
                work.url = form.url.data
                work.description = form.description.data
                work.rating = form.rating.data
                work.spoilers = form.spoilers.data
                work.complete = form.complete.data
                work.sticky = form.sticky.data

                work.word_count = form.word_count.data
                work.status = form.status.data
                work.setting = form.setting.data

                # Let the fun begin...
                form_fandoms = set()
                form_ships = set()
                form_authors = set()
                form_tags = set()

                # Fandoms:
                for entry in request.form.getlist('fandoms'):
                    fandom = Fandom.query.filter(Fandom.id == int(entry)).first()
                    if not fandom:
                        raise MalformedRequestException()
                    form_fandoms.add(fandom)

                for fandom in work.fandoms:
                    if fandom not in form_fandoms:
                        # Existed in work before, not any longer
                        work.fandoms.remove(fandom)
                    else:
                        # Already exists in work
                        form_fandoms.remove(fandom)

                for fandom in form_fandoms:
                    # Only fandoms that aren't already in work will be listed here
                    work.fandoms.append(fandom)

                # Ships:
                for entry in request.form.getlist('ships'):
                    ship = Relationship.query.filter(Relationship.id == int(entry)).first()
                    if not ship:
                        raise MalformedRequestException()
                    form_ships.add(ship)

                for ship in work.ships:
                    if ship not in form_ships:
                        # Existed in work before, not any longer
                        work.ships.remove(ship)
                    else:
                        # Already exists in work
                        form_ships.remove(ship)

                for ship in form_ships:
                    # Only ships that aren't already in work will be listed here
                    work.ships.append(ship)

                # Authors:
                for entry in request.form.getlist('authors'):
                    author = Author.query.filter(Author.id == int(entry)).first()
                    if not author:
                        raise MalformedRequestException()
                    form_authors.add(author)

                for author in work.authors:
                    if author not in form_authors:
                        # Existed in work before, not any longer
                        work.authors.remove(author)
                    else:
                        # Already exists in work
                        form_authors.remove(author)

                for author in form_authors:
                    # Only authors that aren't already in work will be listed here
                    work.authors.append(author)

                # Tags:
                tags = form.tags.data
                if tags != '' and tags is not None:
                    # Tags entered
                    l_tags = tags.split(',')
                    for tag in l_tags:
                        tag = tag.strip()
                        db_tag = Tag.query.filter(Tag.name == tag).first()
                        if not db_tag:
                            # New tag
                            db_tag = Tag(tag)
                            db.session.add(db_tag)
                        form_tags.add(db_tag)

                    for tag in work.tags:
                        if tag not in form_tags:
                            # Existed in work before, not any longer
                            work.tags.remove(tag)
                        else:
                            # Already exists in work
                            form_tags.remove(tag)

                    for tag in form_tags:
                        # Only tags that aren't already in work will be listed here
                        work.tags.append(tag)

                flash('Work edited', category='info')

            db.session.commit()

            return redirect(url_for('editor_work'))

        return _editor_render_template('works_edit.html', form=form, id=work_id, min_fandoms=self.min_fandoms,
                                       min_ships=self.min_ships, min_authors=self.min_authors, form_extra=form_extra,
                                       s_form_extra=self.serializer.dumps(form_extra))

        # Exit hell ==>
        # Please give Kerboros your cookie. Oh, you ate your cookie? Kerboros can still eat it...


def _editor_menu():
    menu = Markup(u'')
    base_menu = Markup(u'<li><a href="{0}">{1}</a></li>\n')

    for item, endpoint in [
        ('Fandoms', EditorFandom.endpoint),
        ('Characters', EditorCharacter.endpoint),
        ('Ships', EditorShips.endpoint),
        ('Authors', EditorAuthor.endpoint),
        ('Works', EditorWork.endpoint)
    ]:
        menu += base_menu.format(url_for(endpoint), item)

    return menu


def _editor_render_template(template_name, **kwargs):
    if 'menu' in kwargs:
        return render_template(template_name, **kwargs)
    else:
        return render_template(template_name, menu=_editor_menu(), **kwargs)


# API Calls ============================================================================================================
class APIFandom(MethodView):
    endpoint = 'api_fandom'

    def get(self):
        fandoms = Fandom.query.order_by('name').all()
        data = []
        for fandom in fandoms:
            data.append({
                'id': fandom.id,
                'name': fandom.name,  # TODO: Make name markup safe by default, unless turned off
                'comments': fandom.comments
            })
        response = make_response(json.dumps(data))
        response.mimetype = 'application/json'
        return response


class APIActiveFandom(MethodView):
    endpoint = 'api_active_fandom'

    def get(self):
        fandoms = db.session.query(Fandom).join(fandoms_works).order_by(Fandom.name).all()
        data = []
        for fandom in fandoms:
            data.append({
                'id': fandom.id,
                'name': fandom.name  # TODO: Make name markup safe by default
            })
        response = make_response(json.dumps(data))
        response.mimetype = 'application/json'
        return response


class APICharacters(MethodView):
    endpoint = 'api_characters'

    def get(self, fandom_id=None):
        data = []
        if fandom_id is not None:
            fandoms = Fandom.query.filter(Fandom.id == fandom_id).all()
        else:
            fandoms = Fandom.query.all()

        for fandom in fandoms:
            fandom_data = {
                'id': fandom.id,
                'name': fandom.name,
                'characters': []
            }
            for character in fandom.characters:
                fandom_data['characters'].append(
                    {
                        'id': character.id,
                        'name': character.name
                    }
                )
            data.append(fandom_data)

        response = make_response(json.dumps(data))
        response.mimetype = 'application/json'
        return response


def _parse_ships_to_response(ships):
    data = []
    for ship in ships:
        ship_data = {
            'id': ship.id,
            'name': ship.name,
            'comments': ship.comments,
            'characters': []
        }
        for character in ship.characters:
            ship_data['characters'].append({
                'id': character.id,
                'name': character.name
            })
        data.append(ship_data)

    response = make_response(json.dumps(data))
    response.mimetype = 'application/json'
    return response


class APIShips(MethodView):
    endpoint = 'api_ships'

    def get(self):
        ships = Relationship.query.all()
        return _parse_ships_to_response(ships)


class APIShipsByFandom(MethodView):
    endpoint = 'api_ships_by_fandom'

    def get(self, fandom_id):
        ships = db.session.query(Relationship).join(Relationship.fandoms).filter(Fandom.id == fandom_id).all()
        return _parse_ships_to_response(ships)


class APIShipsByCharacter(MethodView):
    endpoint = 'api_ships_by_character'

    def get(self, character_id):
        ships = db.session.query(Relationship).join(Relationship.characters).filter(Character.id == character_id).all()
        return _parse_ships_to_response(ships)


def _parse_works_to_response(works_sticky, works):
    data = []
    for dataset in [works_sticky, works]:
        for work in dataset:
            work_data = {
                'id': work.id,
                'title': work.title,
                'description': work.description,
                'url': work.url,
                'spoilers': work.spoilers,
                'complete': work.complete,
                'status': work.status,
                'setting': work.setting,
                'word_count': work.word_count,
                'word_count_readable': u'{0:,}'.format(work.word_count),
                'fandoms': [],
                'ships': [],
                'tags': [],
                'authors': []
            }

            if work.rating == 'UR':
                work_data['rating'] = 'Not rated'
            elif work.rating == 'G':
                work_data['rating'] = 'Gen'
            else:
                work_data['rating'] = work.rating

            for fandom in work.fandoms:
                work_data['fandoms'].append({
                    'id': fandom.id,
                    'name': fandom.name
                })

            for ship in work.ships:
                work_data['ships'].append({
                    'id': ship.id,
                    'name': ship.name,
                    'characters': [
                        {
                            'id': c.id,
                            'name': c.name
                        } for c in ship.characters
                        ]
                })
            for tag in work.tags:
                work_data['tags'].append({
                    'id': tag.id,
                    'name': tag.name
                })
            for author in work.authors:
                work_data['authors'].append({
                    'id': author.id,
                    'name': author.name,
                    'url': author.url
                })
            data.append(work_data)

    response = make_response(json.dumps(data))
    response.mimetype = 'application/json'
    return response


class APIWorks(MethodView):
    endpoint = 'api_works'

    def get(self):
        # works = Work.query.all()
        works_sticky = Work.query.filter(Work.sticky).all()
        works = Work.query.filter(Work.sticky == false()).all()
        return _parse_works_to_response(works_sticky, works)


class APIWorksSortByWordCount(MethodView):
    endpoint = 'api_works_sort_by_word_count'

    def get(self):
        works = Work.query.order_by(Work.word_count).all()
        return _parse_works_to_response(works)


class APIWorksByShip(MethodView):
    endpoint = 'api_works_by_ship'

    def get(self, ship_id):
        works = db.session.query(Work).join(Work.ships).filter(Relationship.id == ship_id).all()
        return _parse_works_to_response(works)


class APIWorksByShipSortByWordCount(MethodView):
    endpoint = 'api_works_by_ship_sort_by_word_count'

    def get(self, ship_id):
        works = db.session.query(Work).join(Work.ships).filter(Relationship.id == ship_id).order_by(Work.word_count
                                                                                                    ).all()
        return _parse_works_to_response(works)


class APIWorksByFandom(MethodView):
    endpoint = 'api_works_by_fandom'

    def get(self, fandom_id):
        works_sticky = db.session.query(Work).join(Work.fandoms).filter(Work.sticky).filter(
            Fandom.id == fandom_id
        ).all()
        works = db.session.query(Work).join(Work.fandoms).filter(Work.sticky == false()).filter(
            Fandom.id == fandom_id
        ).all()
        return _parse_works_to_response(works_sticky, works)


class APIWorksByFandomSortByWordCount(MethodView):
    endpoint = 'api_works_by_fandom_sort_by_word_count'

    def get(self, fandom_id):
        works = db.session.query(Work).join(Work.fandoms).filter(Fandom.id == fandom_id).order_by(Work.word_count).all()
        return _parse_works_to_response(works)


class APIAuthorsByFandom(MethodView):
    endpoint = 'api_authors_by_fandom'

    def get(self, fandom_id):
        authors = db.session.query(Author).join(Author.fandoms).filter(Fandom.id == fandom_id).all()

        data = []
        for author in authors:
            author_data = {
                'id': author.id,
                'name': author.name,
                'url': author.url,
                'works': []
            }
            for work in author.works:
                author_data['works'].append({
                    'id': work.id,
                    'title': work.title,
                    'url': work.url
                })
            data.append(author_data)

        response = make_response(json.dumps(data))
        response.mimetype = 'application/json'

        return response
