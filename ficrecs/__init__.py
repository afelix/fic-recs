# encoding=utf-8
from __future__ import absolute_import

from flask import Flask

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_jsglue import JSGlue

from ficrecs.config import metadata

app = Flask(__name__)
app.config.from_object('ficrecs.config')
db = SQLAlchemy(app, metadata=metadata)

# from ficrecs.database import db_session, init_db
from ficrecs.views import (
    Index,
    EditorIndex, EditorLogin, EditorLogout, EditorFandom, EditorFandomEdit, EditorCharacter, EditorCharacterEdit,
    EditorShips, EditorShipsEdit, EditorShipsEdit, EditorAuthor, EditorAuthorEdit, EditorWork, EditorWorkEdit,
    APIFandom, APIActiveFandom, APICharacters, APIShips, APIShipsByCharacter, APIShipsByFandom, APIWorks,
    APIWorksByShip, APIWorksByFandom, APIAuthorsByFandom, APIWorksSortByWordCount, APIWorksByShipSortByWordCount,
    APIWorksByFandomSortByWordCount
)
from ficrecs.models import User

__author__ = 'Arlena <arlena@hubsec.eu>'


# init_db()

jsglue = JSGlue(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = EditorLogin.endpoint
login_manager.login_message = u'Please login before viewing this page'
login_manager.login_message_category = 'error'

app.add_url_rule('/', endpoint=Index.endpoint, view_func=Index.as_view(Index.endpoint), methods=['GET'])

# Editor ===============================================================================================================
app.add_url_rule('/editor/', endpoint=EditorIndex.endpoint, view_func=EditorIndex.as_view(EditorIndex.endpoint),
                 methods=['GET'])
app.add_url_rule('/editor/login', endpoint=EditorLogin.endpoint, view_func=EditorLogin.as_view(EditorLogin.endpoint),
                 methods=['GET', 'POST'])
app.add_url_rule('/editor/logout', endpoint=EditorLogout.endpoint,
                 view_func=EditorLogout.as_view(EditorLogout.endpoint), methods=['GET'])
app.add_url_rule('/editor/fandom', endpoint=EditorFandom.endpoint,
                 view_func=EditorFandom.as_view(EditorFandom.endpoint), methods=['GET'])

editor_fandom_edit_view = EditorFandomEdit.as_view(EditorFandomEdit.endpoint)
app.add_url_rule('/editor/fandom/edit', endpoint=EditorFandomEdit.endpoint, defaults={'fandom_id': -1},
                 view_func=editor_fandom_edit_view, methods=['GET', 'POST'])
app.add_url_rule('/editor/fandom/edit/<int:fandom_id>', endpoint=EditorFandomEdit.endpoint, methods=['GET', 'POST'],
                 view_func=editor_fandom_edit_view)

app.add_url_rule('/editor/character', endpoint=EditorCharacter.endpoint,
                 view_func=EditorCharacter.as_view(EditorCharacter.endpoint), methods=['GET'])

editor_character_edit_view = EditorCharacterEdit.as_view(EditorCharacterEdit.endpoint)
app.add_url_rule('/editor/character/edit', endpoint=EditorCharacterEdit.endpoint, defaults={'character_id': -1},
                 view_func=editor_character_edit_view, methods=['GET', 'POST'])
app.add_url_rule('/editor/character/edit/<int:character_id>', endpoint=EditorCharacterEdit.endpoint,
                 methods=['GET', 'POST'], view_func=editor_character_edit_view)

app.add_url_rule('/editor/ship', endpoint=EditorShips.endpoint, view_func=EditorShips.as_view(EditorShips.endpoint),
                 methods=['GET'])

editor_ship_edit_view = EditorShipsEdit.as_view(EditorShipsEdit.endpoint)
app.add_url_rule('/editor/ship/edit', endpoint=EditorShipsEdit.endpoint, defaults={'ship_id': -1},
                 view_func=editor_ship_edit_view, methods=['GET', 'POST'])
app.add_url_rule('/editor/ship/edit/<int:ship_id>', endpoint=EditorShipsEdit.endpoint, methods=['GET', 'POST'],
                 view_func=editor_ship_edit_view)

app.add_url_rule('/editor/author', endpoint=EditorAuthor.endpoint, methods=['GET'],
                 view_func=EditorAuthor.as_view(EditorAuthor.endpoint))

editor_author_edit_view = EditorAuthorEdit.as_view(EditorAuthorEdit.endpoint)
app.add_url_rule('/editor/author/edit', endpoint=EditorAuthorEdit.endpoint, defaults={'author_id': -1},
                 view_func=editor_author_edit_view, methods=['GET', 'POST'])
app.add_url_rule('/editor/author/edit/<int:author_id>', endpoint=EditorAuthorEdit.endpoint, methods=['GET', 'POST'],
                 view_func=editor_author_edit_view)

app.add_url_rule('/editor/work', endpoint=EditorWork.endpoint, methods=['GET'],
                 view_func=EditorWork.as_view(EditorWork.endpoint))

editor_work_edit_view = EditorWorkEdit.as_view(EditorWorkEdit.endpoint)
app.add_url_rule('/editor/work/edit', endpoint=EditorWorkEdit.endpoint, defaults={'work_id': -1},
                 view_func=editor_work_edit_view, methods=['GET', 'POST'])
app.add_url_rule('/editor/work/edit/<int:work_id>', endpoint=EditorWorkEdit.endpoint, methods=['GET', 'POST'],
                 view_func=editor_work_edit_view)


# API ==================================================================================================================
app.add_url_rule('/api/fandoms', endpoint=APIFandom.endpoint, view_func=APIFandom.as_view(APIFandom.endpoint),
                 methods=['GET'])
app.add_url_rule('/api/fandoms/active', endpoint=APIActiveFandom.endpoint, methods=['GET'],
                 view_func=APIActiveFandom.as_view(APIActiveFandom.endpoint))

api_characters_view = APICharacters.as_view(APICharacters.endpoint)
app.add_url_rule('/api/characters', endpoint=APICharacters.endpoint, methods=['GET'], defaults={'fandom_id': None},
                 view_func=api_characters_view)
app.add_url_rule('/api/characters/<int:fandom_id>', endpoint=APICharacters.endpoint, methods=['GET'],
                 view_func=api_characters_view)

app.add_url_rule('/api/ships', endpoint=APIShips.endpoint, view_func=APIShips.as_view(APIShips.endpoint),
                 methods=['GET'])
app.add_url_rule('/api/ships/fandom/<int:fandom_id>', endpoint=APIShipsByFandom.endpoint, methods=['GET'],
                 view_func=APIShipsByFandom.as_view(APIShipsByFandom.endpoint))
app.add_url_rule('/api/ships/character/<int:character_id>', endpoint=APIShipsByCharacter.endpoint, methods=['GET'],
                 view_func=APIShipsByCharacter.as_view(APIShipsByCharacter.endpoint))

app.add_url_rule('/api/works', endpoint=APIWorks.endpoint, view_func=APIWorks.as_view(APIWorks.endpoint),
                 methods=['GET'])
app.add_url_rule('/api/works/sort/words', endpoint=APIWorksSortByWordCount.endpoint, methods=['GET'],
                 view_func=APIWorksSortByWordCount.as_view(APIWorksSortByWordCount.endpoint))

app.add_url_rule('/api/works/ship/<int:ship_id>', endpoint=APIWorksByShip.endpoint, methods=['GET'],
                 view_func=APIWorksByShip.as_view(APIWorksByShip.endpoint))
app.add_url_rule('/api/works/ship/<int:ship_id>/sort/words', endpoint=APIWorksByShipSortByWordCount.endpoint,
                 view_func=APIWorksByShipSortByWordCount.as_view(APIWorksByShipSortByWordCount.endpoint),
                 methods=['GET'])

app.add_url_rule('/api/works/fandom/<int:fandom_id>', endpoint=APIWorksByFandom.endpoint, methods=['GET'],
                 view_func=APIWorksByFandom.as_view(APIWorksByFandom.endpoint))
app.add_url_rule('/api/works/fandom/<int:fandom_id>/sort/words', endpoint=APIWorksByFandomSortByWordCount.endpoint,
                 methods=['GET'],
                 view_func=APIWorksByFandomSortByWordCount.as_view(APIWorksByFandomSortByWordCount.endpoint))

app.add_url_rule('/api/authors/fandom/<int:fandom_id>', endpoint=APIAuthorsByFandom.endpoint, methods=['GET'],
                 view_func=APIAuthorsByFandom.as_view(APIAuthorsByFandom.endpoint))


# API 2.0
from ficrecs.api import (
    api,
    FandomList, FandomDetail, FandomRelationship,
    CharacterList, CharacterDetail, CharacterRelationship,
    ShipList, ShipDetail, ShipRelationship,
    AuthorList, AuthorDetail, AuthorRelationship,
    TagList, TagDetail, TagRelationship,
    WorkList, WorkDetail, WorkRelationship
)
api.init_app(app)
api.route(FandomList, 'fandom_list', '/api/2/fandoms')
api.route(FandomDetail, 'fandom_detail', '/api/2/fandoms/<int:id>')
api.route(FandomRelationship, 'fandom_authors', '/api/2/fandoms/<int:id>/relationships/authors')
api.route(FandomRelationship, 'fandom_works', '/api/2/fandoms/<int:id>/relationships/works')
api.route(FandomRelationship, 'fandom_ships', '/api/2/fandoms/<int:id>/relationships/ships')
api.route(FandomRelationship, 'fandom_characters', '/api/2/fandoms/<int:id>/relationships/characters')

api.route(CharacterList, 'character_list', '/api/2/characters')
api.route(CharacterDetail, 'character_detail', '/api/2/characters/<int:id>')
api.route(CharacterRelationship, 'character_fandom', '/api/2/characters/<int:id>/relationships/fandom')
api.route(CharacterRelationship, 'character_ships', '/api/2/characters/<int:id>/relationships/ships')

api.route(ShipList, 'ship_list', '/api/2/ships')
api.route(ShipDetail, 'ship_detail', '/api/2/ships/<int:id>')
api.route(ShipRelationship, 'ship_characters', '/api/2/ships/<int:id>/relationships/characters')
api.route(ShipRelationship, 'ship_works', '/api/2/ships/<int:id>/relationships/ships')
api.route(ShipRelationship, 'ship_fandoms', '/api/2/ships/<int:id>/relationships/fandoms')

api.route(AuthorList, 'author_list', '/api/2/authors')
api.route(AuthorDetail, 'author_detail', '/api/2/authors/<int:id>')
api.route(AuthorRelationship, 'author_works', '/api/2/authors/<int:id>/relationships/works')
api.route(AuthorRelationship, 'author_fandoms', '/api/2/authors/<int:id>/relationships/fandoms')

api.route(TagList, 'tag_list', '/api/2/tags')
api.route(TagDetail, 'tag_detail', '/api/2/tags/<int:id>')
api.route(TagRelationship, 'tag_works', '/api/2/tags/<int:id>/relationships/tags')

api.route(WorkList, 'work_list', '/api/2/works')
api.route(WorkDetail, 'work_detail', '/api/2/works/<int:id>')
api.route(WorkRelationship, 'work_fandoms', '/api/2/works/<int:id>/relationships/fandoms')
api.route(WorkRelationship, 'work_ships', '/api/2/works/<int:id>/relationships/ships')
api.route(WorkRelationship, 'work_tags', '/api/2/works/<int:id>/relationships/tags')
api.route(WorkRelationship, 'work_authors', '/api/2/works/<int:id>/relationships/authors')


@login_manager.user_loader
def load_user(user_id):
    try:
        return User.query.filter(User.id == int(user_id)).first()
    except (ValueError, AttributeError):
        return None


if __name__ == '__main__':
    app.run(debug=True)
