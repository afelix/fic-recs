# encoding=utf-8
class MalformedRequestException(Exception):
    status_code = 400

    def __init__(self, status_code=None):
        super(MalformedRequestException, self).__init__()
        if status_code is not None:
            self.status_code = status_code
