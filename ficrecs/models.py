# encoding=utf-8
from __future__ import absolute_import

from hashlib import sha512

# from sqlalchemy import Table, Column, Integer, String, Text, Boolean, ForeignKey, ForeignKeyConstraint
# from sqlalchemy.orm import relationship

from flask_login import UserMixin

from ficrecs import db
from ficrecs.config import metadata

# from ficrecs.database import Base


characters_ships = db.Table(
    'characters_ships',
    metadata,
    db.Column('character_id', db.Integer, db.ForeignKey('characters.id', ondelete='CASCADE')),
    db.Column('ship_id', db.Integer, db.ForeignKey('ships.id', ondelete='CASCADE'))
)

fandoms_works = db.Table(
    'fandoms_works',
    metadata,
    db.Column('fandom_id', db.Integer, db.ForeignKey('fandoms.id', ondelete='CASCADE')),
    db.Column('work_id', db.Integer, db.ForeignKey('works.id', ondelete='CASCADE'))
)

fandoms_ships = db.Table(
    'fandoms_ships',
    metadata,
    db.Column('fandom_id', db.Integer, db.ForeignKey('fandoms.id', ondelete='CASCADE')),
    db.Column('ship_id', db.Integer, db.ForeignKey('ships.id', ondelete='CASCADE'))
)

fandom_authors = db.Table(
    'fandoms_authors',
    metadata,
    db.Column('fandom_id', db.Integer, db.ForeignKey('fandoms.id', ondelete='CASCADE')),
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id', ondelete='CASCADE'))
)


works_ships = db.Table(
    'works_ships',
    metadata,
    db.Column('work_id', db.Integer, db.ForeignKey('works.id')),
    db.Column('ship_id', db.Integer, db.ForeignKey('ships.id'))
)

works_tags = db.Table(
    'works_tags',
    metadata,
    db.Column('work_id', db.Integer, db.ForeignKey('works.id')),
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'))
)

works_authors = db.Table(
    'works_authors',
    metadata,
    db.Column('work_id', db.Integer, db.ForeignKey('works.id')),
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id'))
)


class Fandom(db.Model):
    __tablename__ = 'fandoms'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(length=255), unique=True, nullable=False)
    comments = db.Column('comments', db.Text, nullable=True)

    authors = db.relationship('Author', secondary=fandom_authors, back_populates='fandoms')
    works = db.relationship('Work', secondary=fandoms_works, back_populates='fandoms')
    ships = db.relationship('Relationship', secondary=fandoms_ships, back_populates='fandoms')
    characters = db.relationship('Character')

    def __init__(self, name, comments=None):
        self.name = name
        self.comments = comments

    def __repr__(self):
        return u'<{0} - {1}>'.format(self.__class__.__name__, self.name)


class Character(db.Model):
    __tablename__ = 'characters'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(length=255), nullable=False)

    fandom_id = db.Column('fandom_id', db.Integer, db.ForeignKey('fandoms.id'), nullable=False)

    ships = db.relationship('Relationship', secondary=characters_ships, back_populates='characters')

    def __init__(self, name, fandom_id):
        self.name = name
        self.fandom_id = fandom_id


class Relationship(db.Model):
    __tablename__ = 'ships'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(length=255), nullable=True)
    comments = db.Column('comment', db.Text, nullable=True)

    characters = db.relationship('Character', secondary=characters_ships, back_populates='ships')
    works = db.relationship('Work', secondary=works_ships, back_populates='ships')
    fandoms = db.relationship('Fandom', secondary=fandoms_ships, back_populates='ships')

    def __init__(self, name=None, comments=None):
        self.name = name
        self.comments = comments


class Tag(db.Model):
    __tablename__ = 'tags'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(length=255), unique=True, nullable=False, index=True)

    works = db.relationship('Work', secondary=works_tags, back_populates='tags')

    def __init__(self, name):
        self.name = name


class Author(db.Model):
    __tablename__ = 'authors'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(length=255), nullable=False)
    url = db.Column('url', db.Text, nullable=True)
    comments = db.Column('comments', db.Text, nullable=True)

    works = db.relationship('Work', secondary=works_authors, back_populates='authors')
    fandoms = db.relationship('Fandom', secondary=fandom_authors, back_populates='authors')

    def __init__(self, name, url=None, comments=None):
        self.name = name
        self.url = url
        self.comments = comments


class Work(db.Model):
    __tablename__ = 'works'
    id = db.Column('id', db.Integer, primary_key=True)
    title = db.Column('title', db.String(length=255), nullable=False)
    url = db.Column('url', db.Text, nullable=False)
    description = db.Column('description', db.Text, nullable=True)
    spoilers = db.Column('spoilers', db.Text, nullable=True)
    rating = db.Column('rating', db.String(length=5), nullable=False)
    complete = db.Column('complete', db.Boolean, default=False)

    sticky = db.Column('sticky', db.Boolean, default=False)

    word_count = db.Column('word_count', db.Integer, nullable=True)
    status = db.Column('work_status', db.String(length=255), nullable=True)
    setting = db.Column('setting', db.String(length=255), nullable=True)
    # TODO: Add chapter count

    fandoms = db.relationship('Fandom', secondary=fandoms_works, back_populates='works')
    ships = db.relationship('Relationship', secondary=works_ships, back_populates='works')
    tags = db.relationship('Tag', secondary=works_tags, back_populates='works', order_by='Tag.id')
    authors = db.relationship('Author', secondary=works_authors,  back_populates='works')

    def __init__(self, title, url, rating, description=None, spoilers=None, complete=False, n_words=None,
                 status=None, setting=None, sticky=None):
        self.title = title
        self.url = url
        self.description = description
        self.spoilers = spoilers
        self.complete = complete
        self.rating = rating

        self.sticky = sticky

        self.word_count = n_words
        self.status = status
        self.setting = setting


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column('id', db.Integer, primary_key=True)
    email = db.Column('email', db.String(length=255), nullable=False, unique=True)
    password = db.Column('password', db.String(length=255), nullable=False)
    salt = db.Column('salt', db.String(length=255), nullable=False)
    name = db.Column('name', db.String(length=255), nullable=False)

    def __init__(self, email, password, salt, name):
        self.email = email
        self.password = password
        self.salt = salt
        self.name = name

    def __repr__(self):
        return u'<{0} - {1!r}>'.format(self.__class__.__name__, self.name)

    def validate_password(self, password):
        return self.password == sha512(self.salt + password + 't_>IyP[dZ=;1ZKB~GLW(UH54]^$8LG$imX`ZX%Gh').hexdigest()
