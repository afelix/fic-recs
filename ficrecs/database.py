# coding=utf-8
from __future__ import absolute_import

from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from ficrecs.config import db_info

__author__ = 'Arlena <arlena@hubsec.eu>'

database_uri = 'mysql+pymysql://{0[username]}:{0[password]}@{0[host]}:{0[port]}/{0[db_name]}?charset=utf8'\
    .format(db_info)
engine = create_engine(database_uri)
db_session = scoped_session(sessionmaker(autoflush=False, bind=engine))

Base = declarative_base(metadata=MetaData(
    naming_convention={
        'pk': 'pk_%(table_name)s',
        'fk': 'fk_%(table_name)s_%(referred_table_name)s_%(referred_column_0_name)s',
        'uq': 'uq_%(table_name)s_%(column_0_name)s'
    }
))
Base.query = db_session.query_property()


def init_db():
    import ficrecs.models
    Base.metadata.create_all(bind=engine)
