# encoding=utf-8
from flask_rest_jsonapi import Api, ResourceList, ResourceDetail, ResourceRelationship
from flask_login import login_required

from ficrecs.schemas import FandomSchema, CharacterSchema, ShipSchema, AuthorSchema, WorkSchema, TagSchema
from ficrecs.models import Fandom, Character, Relationship, Author, Work, Tag
from ficrecs import db


api = Api()


class FandomList(ResourceList):
    schema = FandomSchema
    data_layer = {
        'session': db.session,
        'model': Fandom
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class FandomDetail(ResourceDetail):
    schema = FandomSchema
    data_layer = {
        'session': db.session,
        'model': Fandom
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class FandomRelationship(ResourceRelationship):
    schema = FandomSchema
    data_layer = {
        'session': db.session,
        'model': Fandom
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class CharacterList(ResourceList):
    schema = CharacterSchema
    data_layer = {
        'session': db.session,
        'model': Character
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class CharacterDetail(ResourceDetail):
    schema = CharacterSchema
    data_layer = {
        'session': db.session,
        'model': Character
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class CharacterRelationship(ResourceRelationship):
    schema = CharacterSchema
    data_layer = {
        'session': db.session,
        'model': Character
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class ShipList(ResourceList):
    schema = ShipSchema
    data_layer = {
        'session': db.session,
        'model': Relationship
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class ShipDetail(ResourceDetail):
    schema = ShipSchema
    data_layer = {
        'session': db.session,
        'model': Relationship
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class ShipRelationship(ResourceRelationship):
    schema = ShipSchema
    data_layer = {
        'session': db.session,
        'model': Relationship
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class AuthorList(ResourceList):
    schema = AuthorSchema
    data_layer = {
        'session': db.session,
        'model': Author
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class AuthorDetail(ResourceDetail):
    schema = AuthorSchema
    data_layer = {
        'session': db.session,
        'model': Author
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class AuthorRelationship(ResourceRelationship):
    schema = AuthorSchema
    data_layer = {
        'session': db.session,
        'model': Author
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class TagList(ResourceList):
    schema = TagSchema
    data_layer = {
        'session': db.session,
        'model': Tag
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class TagDetail(ResourceDetail):
    schema = TagSchema
    data_layer = {
        'session': db.session,
        'model': Tag
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class TagRelationship(ResourceRelationship):
    schema = TagSchema
    data_layer = {
        'session': db.session,
        'model': Tag
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class WorkList(ResourceList):
    schema = WorkSchema
    data_layer = {
        'session': db.session,
        'model': Work
    }

    @login_required
    def before_post(self, args, kwargs):
        pass


class WorkDetail(ResourceDetail):
    schema = WorkSchema
    data_layer = {
        'session': db.session,
        'model': Work
    }

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass


class WorkRelationship(ResourceRelationship):
    schema = WorkSchema
    data_layer = {
        'session': db.session,
        'model': Work
    }

    @login_required
    def before_post(self, args, kwargs):
        pass

    @login_required
    def before_patch(self, args, kwargs):
        pass

    @login_required
    def before_delete(self, args, kwargs):
        pass
