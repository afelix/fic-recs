# encoding=utf-8
from marshmallow_jsonapi.flask import Schema, Relationship
from marshmallow import fields


class FandomSchema(Schema):
    class Meta:
        type_ = 'fandom'
        self_view = 'fandom_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        self_view_many = 'fandom_list'

    id = fields.Str(dump_only=True)
    name = fields.Str(required=True)
    comments = fields.Str(allow_none=True)

    authors = Relationship(
        self_view='fandom_authors',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='author_list',
        many=True,
        schema='AuthorSchema',
        type_='author'
    )
    works = Relationship(
        self_view='fandom_works',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='work_list',
        many=True,
        schema='WorkSchema',
        type_='work'
    )
    ships = Relationship(
        self_view='fandom_ships',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='ship_list',
        many=True,
        schema='ShipSchema',
        type_='ship'
    )
    characters = Relationship(
        self_view='fandom_characters',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='character_list',
        many=True,
        schema='CharacterSchema',
        type_='character'
    )


class CharacterSchema(Schema):
    class Meta:
        type_ = 'character'
        self_view = 'character_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        self_view_many = 'character_list'

    id = fields.Str(dump_only=True)
    name = fields.Str(required=True)

    fandom = Relationship(
        self_view='character_fandom',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='fandom_list',
        schema='FandomSchema',
        type_='fandom'
    )
    ships = Relationship(
        self_view='character_ships',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='ship_list',
        many=True,
        schema='ShipSchema',
        type_='ship'
    )


class ShipSchema(Schema):
    class Meta:
        type_ = 'ship'
        self_view = 'ship_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        selv_view_many = 'ship_list'

    id = fields.Str(dump_only=True)
    name = fields.Str(allow_none=True)
    comments = fields.Str(allow_none=True)

    characters = Relationship(
        self_view='ship_characters',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='character_list',
        many=True,
        schema='CharacterSchema',
        type_='character',
        include_resource_linkage=True
    )
    works = Relationship(
        self_view='ship_works',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='work_list',
        many=True,
        schema='WorkSchema',
        type_='work'
    )
    fandoms = Relationship(
        self_view='ship_fandoms',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='fandom_list',
        many=True,
        schema='FandomSchema',
        type_='fandom',
        include_resource_linkage=True
    )


class AuthorSchema(Schema):
    class Meta:
        type_ = 'author'
        self_view = 'author_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        self_view_many = 'author_list'

    id = fields.Str(dump_only=True)
    name = fields.Str(required=True)
    url = fields.Url(allow_none=True)
    comments = fields.Str(allow_none=True)

    works = Relationship(
        self_view='author_works',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='work_list',
        many=True,
        schema='WorkSchema',
        type_='work'
    )
    fandoms = Relationship(
        self_view='author_fandoms',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='fandom_list',
        many=True,
        schema='FandomSchema',
        type_='fandom',
        include_resource_linkage=True
    )


class TagSchema(Schema):
    class Meta:
        type_ = 'tag'
        self_view = 'tag_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        self_view_many = 'tag_list'

    id = fields.Str(dump_only=True)
    name = fields.Str(required=True)

    works = Relationship(
        self_view='tag_works',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='work_list',
        many=True,
        schema='WorkSchema',
        type_='work',
        include_resource_linkage=True
    )


class WorkSchema(Schema):
    class Meta:
        type_ = 'work'
        self_view = 'work_detail'
        self_view_kwargs = {
            'id': '<id>'
        }
        self_view_many = 'work_list'

    id = fields.Str(dump_only=True)
    title = fields.Str(required=True)
    url = fields.Url(required=True)
    description = fields.Str(required=True)
    spoilers = fields.Str(allow_none=True)
    rating = fields.Str(required=True)
    readable_rating = fields.Method('get_readable_rating')
    complete = fields.Bool(missing=False, default=False)
    status = fields.Str(allow_none=True)
    setting = fields.Str(allow_none=True)
    word_count = fields.Int(allow_none=True)
    readable_word_count = fields.FormattedString('{word_count:,}')
    sticky = fields.Bool(missing=False, default=False)

    fandoms = Relationship(
        self_view='work_fandoms',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='fandom_list',
        many=True,
        schema='FandomSchema',
        type_='fandom',
        include_resource_linkage=True
    )
    ships = Relationship(
        self_view='work_ships',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='ship_list',
        many=True,
        schema='ShipSchema',
        type_='ship',
        include_resource_linkage=True
    )
    tags = Relationship(
        self_view='work_tags',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='tag_list',
        many=True,
        schema='TagSchema',
        type_='tag',
        include_resource_linkage=True
    )
    authors = Relationship(
        self_view='work_authors',
        self_view_kwargs={
            'id': '<id>'
        },
        related_view='author_list',
        many=True,
        schema='AuthorSchema',
        type_='author',
        include_resource_linkage=True
    )

    @staticmethod
    def get_readable_rating(obj):
        if obj.rating == 'UR':
            return 'Not rated'
        elif obj.rating == 'G':
            return 'Gen'
        else:
            return obj.rating
