var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: './frontend/src/main.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'frontend/dist')
    },
    module: {
        rules: [
            {
                test: /.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        js: 'babel-loader'
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components|ficrecs)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['env'], ['es2015', {modules: false}]]
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $ : "jquery",
            jQuery : "jquery",
            "window.jQuery" : "jquery",
            "root.jQuery" : "jquery"
        }),
        // short-circuits all Vue.js warning code
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        // minify with dead-code elimination
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};