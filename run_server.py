# encoding=utf-8
from ficrecs import app
# from ficrecs.database import init_db

__author__ = 'Arlena <arlena@hubsec.eu>'

if __name__ == '__main__':
    # init_db()
    app.run(host='0.0.0.0', port=8156, debug=False)
